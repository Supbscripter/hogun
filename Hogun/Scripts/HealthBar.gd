extends TextureProgress

var player
var newPos

func _ready():
	set_physics_process(1)
	pass

func _physics_process(delta):
	
	player = get_node("../Path2D/PathFollow2D/Player")
	
	# Update NewPOS
	newPos = player.global_position
	newPos.x -= 16
	newPos.y += 22
	
	# Finalize Movement
	rect_position = newPos
	
	# Update HealthBar Value
	value = (player.health / player.maxhealth) * 100