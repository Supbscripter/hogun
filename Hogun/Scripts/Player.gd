extends Node2D

var speed
var health
var maxhealth
var firerate

var canfire = true
var fireratetimer
var path_position = 0

var bulletscene = load("res://Scenes/PlayerBullet.tscn")

# Creates a bullet in the correct position
func _create_bullet(up):
	# Create bullet
	var bullet = bulletscene.instance()
	get_node("/root/World").add_child(bullet)
	
	# Set bullets scale
	bullet.global_scale = global_scale
	
	# Set bullet rotation and position
	if up:
		bullet.global_position = get_node("TopGun").global_position
		bullet.global_rotation_degrees = global_rotation_degrees
	if !up:
		bullet.global_position = get_node("BottomGun").global_position
		bullet.global_rotation_degrees = global_rotation_degrees - 180
	
	# Move bullet to starting position
	bullet.move_local_y(-2)
	pass

func _ready():
	fireratetimer = 0
	
	# Set variables
	speed = 0.001
	firerate = 0.2
	maxhealth = 100
	health = maxhealth
	
	set_physics_process(1);
	pass
	
func _physics_process(delta):
	# Update path_position
	path_position = get_parent().unit_offset
	
	# Timer
	if fireratetimer >= firerate:
		fireratetimer -= firerate
		canfire = true
	
	# Attacks
	if Input.is_action_pressed("fire_up") && canfire:
		_create_bullet(true)
		canfire = false
	if Input.is_action_pressed("fire_down") && canfire:
		_create_bullet(false)
		canfire = false
	
	# Get Movement
	if Input.is_action_pressed("movement_left"):
		path_position -= speed
	if Input.is_action_pressed("movement_right"):
		path_position += speed
	
	# Keep position around path between 0 and 1
	if path_position > 1:
		path_position -= 1
	if path_position < 0:
		path_position += 1
	
	# Finalize Movement
	get_parent().unit_offset = path_position
	
	# Increase timer
	fireratetimer += delta
	pass