extends Node2D

var speed

func _ready():
	speed = 10
	
	set_physics_process(1)
	pass

func _physics_process(delta):	
	var movement = Vector2(0, -1)
	movement *= speed
	move_local_y(movement.y)
	move_local_x(movement.x)
	pass